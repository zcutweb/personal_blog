# kenblog

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


## 以下是博客的总结

### vue.js分页插件的实现
https://www.cnblogs.com/yuqing6/p/7061647.html
https://www.cnblogs.com/jh007/p/6185599.html
http://www.jq22.com/jquery-info16260


### Vue2.0 分页插件pagination使用详细说明
https://blog.csdn.net/ganquanzhong/article/details/80320748

### vue-page 分页插件
https://terryz.gitee.io/vue/#/page

### 一个女前端博客
https://minemine.cc/about
https://blog.csdn.net/zhuoganliwanjin/article/details/81872378

### VUE 分页组件
https://blog.csdn.net/happygirlnan/article/details/80008145

### 在同一个界面实现短按拍照和长按录制视频
https://blog.csdn.net/u010705554/article/details/79743336

### 前端使用html5、ffmpeg实现录屏摄像等功能
https://juejin.im/post/5bee7b39e51d4552da47c97b

### getUserMedia API及HTML5 调用手机摄像头拍照
https://segmentfault.com/a/1190000011793960?utm_source=tag-newest
https://blog.csdn.net/mrzhangdulin/article/details/84560146
https://blog.csdn.net/qq_35840181/article/details/80484672

### 解决了id问题

### 解决路由跳转问题
